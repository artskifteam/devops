# Настройка DevOps сервера

Для настройки сервера мы будем использовать следующие инструменты  

- **Ansible** - как способ оркестрации окружением, т.е. запуском скриптов, установкой и настройкой контейнеров и т.д.  
- **Docker** - как способ контейнеризации окружения  
- **Jenkins**(Docker) - как самый главный инструмент DevOps, ради которого все и задумывалось. А конкретнее CI, CD, Tests, Code Review, Builds сервер  
- **Sonar Qube**(Docker) - как инструмент, который показывает проанализированный код. Проще говоря Code Review  
- **Sonar Scanner**- как инструмент, который умеет проводить анализ кода, отправляя полученные данные в SonarQube веб интерфейс
- **PHPUnit** - как способ тестирования проектов  
- **Composer** - как способ собирать проекты  
- **PHP** - как основной язык программирования  
- **Python** - как вспомогательный язык программирования(для связи Docker-Ansible)
- **Postgres**(Docker) - как БД для SonarQube или ваших проектов
- **MySQL(MariaDB)**(Docker) - как БД для ваших проектов  
- **Nginx**(Docker) - WEB сервер для ваших проектов
- **PHP-FPM**(Docker) - Довольно неплохой контейнер который содержит множество модулей для работы с PHP проекта


## Подготовка к установке

### Установка Ansible

Для начала работы необходимо установить Ansible

 [установка Ansible](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html#latest-releases-via-pip)

Для тех у кого Mac можно просто запустить команду  

```
brew install ansible
```

_Если инсталяция в какой-то момент упадет, можно просто перезупустить Mac и попробовать снова. Почему-то это сработает_

#### Настройка Ansible

Реквизиты подключения к удаленному серверу должны находится в файле hosts.yml. 
Для настройки подключения Ansible к удаленным(или локальным) хостам переименуйте файл **hosts.default.yml** в **hosts.yml** и заполните в нем соответсвующие поля 
Для натройки серверного окружения отредактируйте файл **config.yml** или оставьте настройки по умолчанию

О том как работает и настраивается Ansible окружение читайте [Working with inwentory](https://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html)

#### Запускаем Ansible

Проверяем что Ansible работает и готов к запуску

Внимание! Возможно у вас возникнут проблемы с **sshpass**. Для устранения проблем с sshpass перейдите по [ссылке](https://github.com/ansible-tw/AMA/issues/21) и выполните рекомендации

```
# Пингуем сервера
ansible all -m ping
```

Сказать свое первое Hello World вы можете командой  

```
ansible all -a "/bin/echo Hello world"
```

Запустить Ansible на выполнение можно следующей командой

```
ansible-playbook tasks.yml
```

Можно запустить лишь часть задач запустив команду

```
ansible-playbook tasks.yml --tags "aptupdate,aptdependency"
ansible-playbook tasks.yml --tags "docker-containers" # пересоздать все контейнеры
ansible-playbook tasks.yml --tags "install" # подготовить окружение для установки контейнеров
ansible-playbook tasks.yml --tags "composer" # установить composer
ansible-playbook tasks.yml --tags "jenkins" # установить jenkins
ansible-playbook tasks.yml --tags "postgres" # установить postgres
ansible-playbook tasks.yml --tags "sonarqube" # установить sonarqube
ansible-playbook tasks.yml --tags "getbitbucket" # создать web проект из репозитория
ansible-playbook tasks.yml --tags "sonar-scanner" # установить отдельно sonar-scanner
ansible-playbook tasks.yml --tags "mariadb" # установить базу данных Mysql(MariaDB)
ansible-playbook tasks.yml --tags "nginx" # установить web сервер nginx
ansible-playbook tasks.yml --tags "php-fpm" # установить php-fpm
ansible-playbook tasks.yml --tags "final_build" # сбилдить проект для запуска через web интерфейс
```

Или запустить выполнение, пропустив некоторые шаги  

```
ansible-playbook tasks.yml --skip-tags "aptupdate"
```

## После установки 

### Jenkins

После установки и успешного запуска Ansible, веб интерфейс для Jenkins будет доступен по адресу **[ansible_host]:8080**

Для успешной работы с Jenkins рекомендуется посмотреть какой-нибудь видеокурс, например [этот](https://www.youtube.com/watch?v=89yWXXIOisk&index=1&list=PLhW3qG5bs-L_ZCOA4zNPSoGbnVQ-rp_dG)

Директория где лежит Jenkins

    /var/jenkins_home

### Sonar Qube

После установки и успешного запуска Sonar Qube, веб интерфейс будет доступен по адресу **[ansible_host]:9900**

    /var/sonar_qube

### Sonar Scanner

После установки Sonar Scanner будет лежать в директории на севрере  

    /var/sonar_scanner

### PHP-FPM

После установки PHP-FPM конфиг будет лежать в директории на севрере  

    /var/php-config

### GIT

Ваш проект будет лежать в директории   

    /var/bitbucket/{{ bitbucket_sources_repo_account }}/{{ bitbucket_sources_repo_name }}

Внимание! Jenkins работает с своей внутренней директорией проекта, так что текущая директория является вспомогательной для нужд разработчика

### Mysql(MariaDB)

После установки PMA будет доступен по адресу **[ansible_host]:8282*. Реквизиты доступа вы найдете в конфиге **config.yml**

### Nginx web - сервер

После установки ваш сайт будет доступен по адресу **[ansible_host][:80]*. Реквизиты доступа вы найдете в конфиге **config.yml**

## В помощь разработчику

Посмотреть список слушаемых tcp портов можно командой

    netstat -l -n -t -p

Посмотреть размер всех поддиректорий отсортированных по размеру

    du -h | sort -h
    du -sh # размер текущей директории

Команды докера

```
# все имеющиеся контейнеры в распоряжении
docker ps -a
# статус работы контейнеров
docker stats 
```

Зайти в контейнер можно следующей командой

```
docker exec -i -t [name] /bin/bash
```

Выполнить команду composer изнутри php-fpm контейнера используя пользователя www-data

docker exec -it -u www-data -w /var/www/laravel php-fpm bash -c 'composer create-project --prefer-dist laravel/laravel blog'

### Экспорт бд

Для **экспорта** базы данных(например на локальный тестовый сервер) запустите следущие команды

```
# экспорт структуры таблиц(сначала создаем структуру потом накатываем данные)
mysqldump -u ish -p --no-data ish > database_structure.sql

# экспорт таблиц
mysqldump -u ish -p ish > dump_file.sql
```

Команды создают файлы - дампа структуры базы данных и дампа данных базы, который потом можно будет импортировать в любую БД.  

Для импорта дампа базы данных необходимо запустить команды:  

```
# импорт структуры базы данных
mysql -u homestead -p ish < database_structure.sql 

# импорт базы данных
mysql -u homestead -p ish < dump_file.sql


