---
- name: "Create the {{ nginx_base_directory }} DIRECTORY on the host"
  become: yes
  file:
    path: "{{ nginx_base_directory }}"
    state: 'directory'
    mode: '0755'
  tags:
    - nginx_web_configs

- name: "Create the {{ nginx_base_directory }} logs DIRECTORY on the host"
  become: yes
  file:
    path: "{{ nginx_base_directory }}/logs"
    state: 'directory'
    mode: '0755'
  tags:
    - nginx_web_configs

- name: "Create the {{ nginx_base_directory }} conf.d DIRECTORY on the host"
  become: yes
  file:
    path: "{{ nginx_base_directory }}/conf.d"
    state: 'directory'
    mode: '0755'
  tags:
    - nginx_web_configs

- name: "Create the {{ nginx_web_example_directory }} web example DIRECTORY on the host"
  become: yes
  file:
    path: "{{ nginx_web_example_directory }}"
    state: 'directory'
    mode: '0755'
  tags:
    - nginx_web_configs

- name: 'Put the nginx.conf file in place'
  become: yes
  template:
    src: nginx.conf.j2
    dest: "{{ nginx_base_directory }}/nginx.conf"
    mode: '0644'
  notify: 'restart-docker-nginx'
  tags:
    - nginx_web_configs

- name: 'Put the EXAMPLE web.conf file in web configs place'
  become: yes
  template:
    src: example.conf.j2
    dest: "{{ nginx_base_directory }}/conf.d/example.conf"
    mode: '0644'
  notify: 'restart-docker-nginx'
  tags:
    - nginx_web_configs

- name: "Copy the start EXAMPLE configs to {{ nginx_base_directory }}/conf.d on the host"
  become: yes
  copy:
    src: "web_configs/"
    dest: "{{ nginx_base_directory }}/conf.d"
    mode: '0644'
  notify: 'restart-docker-nginx'
  tags:
    - nginx_web_configs

- name: 'Put the PROJECT web.conf file in web configs place'
  become: yes
  template:
    src: '{{ nginx_web_project_template }}.conf.j2'
    dest: "{{ nginx_base_directory }}/conf.d/{{ nginx_web_project_name }}.conf"
    mode: '0644'
  notify: 'restart-docker-nginx'
  tags:
    - nginx_web_configs

- name: "Copy the start example web files to {{ nginx_web_example_directory }} on the host"
  become: yes
  copy:
    src: "web_example/"
    dest: "{{ nginx_web_example_directory }}"
    mode: '0644'
  notify: 'restart-docker-nginx'
  tags:
    - nginx_web_configs

- name: Kill nginx container
  docker_container:
    name: "{{ nginx_container_name }}"
    image: "nginx:{{ nginx_docker_tag }}"
    state: absent
  tags:
    - docker-nginx
    - docker-containers

- name: 'Start the nginx docker container'
  become: yes
  docker_container:
    image: "nginx:{{ nginx_docker_tag }}"
    name: "{{ nginx_container_name }}"
    memory: '{{ nginx_memory_limit | default(omit) }}'
    cpu_quota: '{{ nginx_cpu_limit | default(omit) }}'
    volumes: '{{ nginx_exposed_volumes }}'
    ports: '{{ nginx_published_ports }}'
    state: 'started'
    restart_policy: 'always'
    log_driver: 'syslog'
    log_options:
      tag: "{{ nginx_container_name }}"
    links:
    - mysql:mysql
    - "{{ php_container_name }}:{{ php_container_name }}"
  tags:
    - docker-nginx
    - docker-containers

- meta: flush_handlers
